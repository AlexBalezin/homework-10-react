import Logo from "../otus_logo.svg";
import { Link } from "react-router-dom";

export default function Register() {
  return (
    <div className="auth_column">
      <div className="auth_block">
        <img className="auth_logo" src={Logo}></img>
        <form className="auth_editors">
          <input
            className="auth_editor"
            name="first_name"
            placeholder="First name"
            required
          />
          <input
            className="auth_editor"
            name="last_name"
            placeholder="Last name"
            required
          />
          <input
            className="auth_editor"
            type="email"
            name="email"
            placeholder="Email"
            required
          />
          <input
            className="auth_editor"
            type="password"
            name="auth_pass"
            placeholder="Password"
            required
          />
          <button className="auth_button" name="form_auth_submit">
            Register
          </button>
          <div className="text_under-submit">
            <a>Already have login and password? </a>
            <Link to="/login" className="text_register_link">
              Sign in
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
}
