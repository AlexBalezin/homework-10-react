import "../styles/Auth.css";
import Logo from "../otus_logo.svg";
import { Link } from "react-router-dom";
import { useState } from "react";

export default function Login() {
  return (
    <div className="auth_column">
      <div className="auth_block">
        <img className="auth_logo" src={Logo}></img>
        <form className="auth_editors" action="#" method="post">
          <div>
            <input
              className="auth_editor"
              type="email"
              name="auth_email"
              placeholder="Enter your email"
              required
            />
          </div>
          <input
            className="auth_editor"
            type="password"
            name="auth_pass"
            placeholder="Password"
            required
          />
          <button
            className="auth_button"
            name="form_auth_submit"
          >
            Submit
          </button>
          <div className="text_under-submit">
            <a>Don't have an account yet? </a>
            <Link to="/register" className="text_register_link">
              Register now
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
}