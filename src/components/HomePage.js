import "../styles/HomePage.css";
import { useDispatch, useSelector } from "react-redux";
import { increment } from "../features/CounterSlice";

export default function HomePage() {
  const count = useSelector((state) => state.counter.value);
  const dispatch = useDispatch();

  const changeTheme = (event) => {
    const form = document.getElementById("form");
    let app = document.getElementById("App");
    app.style.background = form.source.value;
    event.preventDefault();
    dispatch(increment());
  };

  return (
    <div id="main" className="theme">
      <div className="title">Home Page</div>
      <br />
      <form className="choose" id="form">
        <fieldset name="ch">
          <legend>Choose color background</legend>
          <div>
            <input 
              type="radio" 
              id="black" 
              name="source" 
              value="black" />
            <label htmlFor="black">Black</label>
          </div>
          <div>
            <input
              type="radio"
              id="aliceblue"
              name="source"
              value="aliceblue"
            />
            <label htmlFor="aliceblue">Aliceblue</label>
          </div>
          <div>
            <input 
              type="radio" 
              id="white" 
              name="source" 
              value="white" />
            <label htmlFor="white">White</label>
          </div>
          <div>
            <input
              type="radio"
              id="cadetblue"
              name="source"
              value="cadetblue"
            />
            <label htmlFor="cadetblue">Cadetblue</label>
          </div>
          <div>
            <input
              type="radio"
              id="default"
              name="source"
              value="rgb(139, 138, 138)"
            />
            <label htmlFor="default">Default</label>
          </div>
          <br />
          <div>
            <button className="btn btn-success" onClick={changeTheme}>
              Submit color
            </button>
          </div>
          <br />
          <div>Count of change theme: {count}</div>
        </fieldset>
      </form>
    </div>
  );
}
